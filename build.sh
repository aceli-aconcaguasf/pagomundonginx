#!/bin/bash
usage() { colorEcho r "Usage: $0 -v <version number {X.Y.Z}> [-e <environment (default: sandbox)>] [-h]" 1>&2; }
usageAndExit() {  usage;   exit 1; }

colorEcho() {
  local code="\033["
  case "$1" in
  black | bk) color="${code}0;30m" ;;
  red | r) color="${code}1;31m" ;;
  green | g) color="${code}1;32m" ;;
  yellow | y) color="${code}1;33m" ;;
  blue | b) color="${code}1;34m" ;;
  purple | p) color="${code}1;35m" ;;
  cyan | c) color="${code}1;36m" ;;
  gray | gr) color="${code}0;37m" ;;
  *) local text="$1" ;;
  esac
  [ -z "$text" ] && local text="$color${*:2}${code}0m"
  echo -e "$text"
}

while getopts ":h:e:v:p:m:" o; do
  case "$o" in
  h) usageAndExit ;;
  e) ENVIRONMENT=${OPTARG,,} ;;
  v) VERSION=${OPTARG,,} ;;
  *) colorEcho r "" ;;
  esac
done
shift $((OPTIND - 1))

if [[ -z "${ENVIRONMENT}" ]]; then ENVIRONMENT="sandbox"; fi
if [[ -z "${VERSION}" ]]; then usageAndExit; fi

APPLICATION_NAME="${ENVIRONMENT}-pmi-nginx"
NETWORK_NAME="${ENVIRONMENT}-pmi-net"
IMAGE_NAME="${APPLICATION_NAME}:${VERSION}"
TAG_NAME="${VERSION}.${ENVIRONMENT}"

tput reset
echo
usage
echo
echo
colorEcho p "--> ENVIRONMENT:"
colorEcho r "    ${ENVIRONMENT}"

echo
colorEcho p "${ENVIRONMENT} NGINX:"
colorEcho b "    Name = ${APPLICATION_NAME}"
colorEcho b "    Version = ${VERSION}"
colorEcho b "    Git tag = ${TAG_NAME}"

echo
colorEcho p "${ENVIRONMENT} Docker:"
colorEcho b "    Image = ${IMAGE_NAME}"
colorEcho b "    Network name = ${NETWORK_NAME}"

echo
read -r -p "--> Press enter to start deploying ${IMAGE_NAME} or CTRL+C to abort"

colorEcho p "--> Building docker NGINX..."
colorEcho b "    ${IMAGE_NAME}"
docker build -t "${IMAGE_NAME}" ./
colorEcho g "--> NGINX built! <--"
echo

colorEcho p "--> Renaming docker NGINX..."
#dockerExists=$(docker images -q "${APPLICATION_NAME}")
if [ "$(docker ps -qa -f name="$APPLICATION_NAME")" ]; then
  colorEcho b "    Docker image '${APPLICATION_NAME}' exists."
  if [ "$(docker container inspect -f '{{.State.Running}}' ${APPLICATION_NAME})" == "true" ]; then
    DOCKER_STOP="docker stop ${APPLICATION_NAME}"
    ($DOCKER_STOP)
    colorEcho b "    Docker '${APPLICATION_NAME}' stopped."
  fi

  APPLICATION_RENAME=$APPLICATION_NAME-$(date +%Y.%m.%d.%H.%M)
  DOCKER_RENAME="docker rename ${APPLICATION_NAME} ${APPLICATION_RENAME}"
  ($DOCKER_RENAME)
  colorEcho b "    Docker '${APPLICATION_NAME}' renamed to ${APPLICATION_RENAME}."
else
  colorEcho b "    Docker '${APPLICATION_NAME}' doesn't exist."
fi

DOCKER_DEPLOY="docker run --name ${APPLICATION_NAME} -p 80:80 --restart unless-stopped --network ${NETWORK_NAME} -v /home/ubuntu/pagomundo-nginx/html:/usr/share/nginx/html -d ${IMAGE_NAME}"
colorEcho p "--> Deploying docker..."

($DOCKER_DEPLOY)
#rm server.*
echo
colorEcho g "--> ${ENVIRONMENT} NGINX deployed! <--"
echo

colorEcho p "--> Creating git tag..."
colorEcho b "    ${TAG_NAME}"
#git tag "${TAG_NAME}"
#git push origin --tags


